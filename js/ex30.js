'use strict';
class MyTodolist {
    constructor(selector) {
        this.selector = selector;
        this.ul = document.querySelector('.myTodolist' + selector + " .todoList");
        this.show_hide = document.querySelector('.myTodolist' + selector + " .show_hide");
        this.tmpl = _.template(document.querySelector('.myTodolist' + selector + ' .tmpl').innerHTML);
        this.myInput = document.querySelector('.myTodolist' + selector + " .myInput");
        document.querySelector('.myTodolist' + selector + ' .add').addEventListener('click', this.addTask.bind(this));
        this.ul.addEventListener('click', this.changeTask.bind(this));
        this.show_hide.addEventListener('click', this.showHide.bind(this));
        const dataList = {
            list: [],
            valShowHide: "",
            textButtonSH: "",
        }
        const localStor = localStorage.getItem('localList');
        this.localList = localStor ? JSON.parse(localStor) : dataList;
        this.loadLocalStor(this.localList);
    }
    setToLocalStor() {
        localStorage.setItem('localList', JSON.stringify(this.localList));
    }
    addTemplTask(valueTask){
        this.ul.innerHTML += this.tmpl({ valueTask })
    }
    addTask() {
        const { value } = this.myInput;
        const msg = "You must write something!";
        !value ? alert(msg) : this.addTemplTask(value);
        this.localList.list.push({ val: value, check: "" });
        this.setToLocalStor();
        this.myInput.value = "";
    }
    changeTask({ target }) {
        const { parentNode } = target;
        const indexTask = [...document.querySelectorAll('.myTodolist' + this.selector + ' li')].indexOf(parentNode);
        if (target.classList.contains("close")) {
            parentNode.remove();
            this.localList.list.splice(indexTask, 1);
        } else {
            parentNode.classList.toggle('checked');
            this.localList.list[indexTask].check = parentNode.getAttribute("class");
        }
        this.setToLocalStor();
    }
    showHide() {
        this.ul.classList.toggle('hide');
        this.show_hide.textContent = this.ul.classList.contains("hide") ? 'show' : 'hide';
        this.localList.valShowHide = this.ul.getAttribute("class");
        this.localList.textButtonSH = this.show_hide.textContent;  
        this.setToLocalStor();
    }
    loadLocalStor({list, valShowHide, textButtonSH}) {
        if (list.length) {
            list.forEach((el, i) => {
                this.addTemplTask(el.val);
                this.ul.children[i].setAttribute('class', el.check);
            });
        }
        if(valShowHide){
            this.ul.className = valShowHide;
            this.show_hide.textContent = textButtonSH;
        }
    }
}
new MyTodolist(".example1");












